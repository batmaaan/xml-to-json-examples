package com.sber.sbertest;

public class Utils {
  public static final String content = "<PropertyList>\n" +
    "    <Property>\n" +
    "        <Name>CommandTimeout</Name>\n" +
    "        <Value>60</Value>\n" +
    "        <Description>Setting the timeout(in seconds)</Description>\n" +
    "        <DefaultValue></DefaultValue>\n" +
    "    </Property>\n" +
    "    <Property>\n" +
    "        <Name>Address</Name>\n" +
    "        <Value>0.0.0.0</Value>\n" +
    "        <Description>ip:port</Description>\n" +
    "        <DefaultValue>60</DefaultValue>\n" +
    "    </Property>\n" +
    "</PropertyList>";
}
