package com.sber.sbertest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sber.sbertest.model.PropertyList;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.XML;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.sber.sbertest.Utils.content;

@Slf4j
@RestController
@AllArgsConstructor
public class Controller {


  @SneakyThrows
  @PostMapping(
    value = "/xml_to_json",
    consumes = MediaType.APPLICATION_XML_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  public PropertyList convertXml(@RequestBody PropertyList body) {
    return body;
  }

  public static void main(String[] args) throws JsonProcessingException {
    JacksonXmlModule module = new JacksonXmlModule();
    module.setDefaultUseWrapper(false);
    XmlMapper xmlMapper = new XmlMapper(module);
    var result = xmlMapper.readValue(content, PropertyList.class);
    System.out.println(result);
  }

}
