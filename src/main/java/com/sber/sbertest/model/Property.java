package com.sber.sbertest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Property {
  @JsonProperty("DefaultValue")
  private Integer defaultValue;
  @JsonProperty("Description")
  private String description;
  @JsonProperty("Value")
  private String value;
  @JsonProperty("Name")
  private String name;
}
